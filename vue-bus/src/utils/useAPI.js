import http from './http'
const post = (url, data = {}) =>
  http.post(
    url,
    data
  )
// const get = (url, params = {}) =>
//   http.get(
//     url,
//     params
//   )

// 接口说明
const getLogin = data => post('/busInfo/loginInfo', data)

const getRegistry = data => post('/busInfo/registry', data)


//接口导出
export default{
    getLogin,
    getRegistry
}
