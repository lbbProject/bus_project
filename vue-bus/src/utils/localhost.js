import http from './http'
const post = (url, data = {}) =>
  http.post(
    url,
    data
  )
// 接口说明
const getCommentByArticleId = data => post('/commentInfo/getCommentByArticleId', data)

const addComment = data =>post('/commentInfo/addComment',data)

const getBuslist = () =>post('/bus/busList')

const getBusParams = data =>post('/bus/busParams',data)

const getProductInfo = data => post('/cart/productInfo',data)

const updateById = data => post('/cart/updateById',data)

const updateByNum = data => post('/cart/updateByNum',data)

const getCartInfo = data => post('/cart/getCartInfo',data)

const deleteByCart = data => post('/cart/deleteByCart',data)

const addCart = data => post("/cart/addCart",data)

const updateCartNumById  = data => post("/cart/updateCartNumById",data)

//接口导出
export default{
    getCommentByArticleId,
    addComment,
    getBuslist,
    getBusParams,
    getProductInfo,
    updateById,
    updateByNum,
    getCartInfo,
    deleteByCart,
    addCart,
    updateCartNumById
}
