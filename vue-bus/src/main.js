import Vue from 'vue'
import App from './App.vue'
import "./assets/css/style.css"
 //import './assets/css/base.css'
import './assets/css/index.css'
import router from "./router"
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

import CommonApi from "./utils/localhost"
Vue.prototype.$commonApi = CommonApi

Vue.use(ElementUI);
Vue.config.productionTip = false


//全局路由钩子守卫
router.beforeEach((to, form, next) => {
    //如果进入到的路由是登录页或者注册页面，则正常展示
    if (to.path == '/login' || to.path == '/register') {
        next();
    }
    //  else if (!(sessionStorage.getItem('token'))) {
    //     next('/login');     //转入login登录页面，登录成功后会将token存入localStorage
    // } 
    else {
        next();
    }
})


new Vue({
    router,
    render: h => h(App),
}).$mount('#app')
