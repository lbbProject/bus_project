import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: "/",
            name: "index",
            redirect: "/home",
            component: require("@/components/index.vue").default,
            children: [
                {
                    path: "/home",
                    name: "home",
                    component: require("@/components/home.vue").default,
                },
                {
                    path: "/page1",
                    name: "page1",
                    component: require("@/components/busPage/page1.vue").default,
                },
                {
                    path: "/page2",
                    name: "page2",
                    component: require("@/components/busPage/page2.vue").default,
                },
                {
                    path: "/page3",
                    name: "page3",
                    component: require("@/components/busPage/page3.vue").default,
                },
                {
                    path: "/page4",
                    name: "page4",
                    component: require("@/components/busPage/page4.vue").default,
                },
                {
                    path: "/page5",
                    name: "page5",
                    component: require("@/components/busPage/page5.vue").default,
                },
                {
                    path: "/page6",
                    name: "page6",
                    component: require("@/components/busPage/page6.vue").default,
                },
                {
                    path: "/page7",
                    name: "page7",
                    component: require("@/components/busPage/page7.vue").default,
                },
                {
                    path: "/cart",
                    name: "cart",
                    component: require("@/components/busPage/cart.vue").default,
                },
                {
                    path: "/address",
                    name: "address",
                    component: require("@/components/busPage/address.vue").default,
                }
            ]
        },
        {
            path: "/login",
            name: "login",
            component: require("@/components/login2/index.vue").default,
        },

    ]
});
