/*
 Navicat Premium Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 50540
 Source Host           : localhost:3306
 Source Schema         : busdata

 Target Server Type    : MySQL
 Target Server Version : 50540
 File Encoding         : 65001

 Date: 28/05/2021 11:42:05
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for businfo
-- ----------------------------
DROP TABLE IF EXISTS `businfo`;
CREATE TABLE `businfo`  (
  `id` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `c_busName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '车名',
  `c_busImage` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '车图片地址',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of businfo
-- ----------------------------
INSERT INTO `businfo` VALUES ('1', '卡宴', NULL);
INSERT INTO `businfo` VALUES ('2', '保时捷', NULL);
INSERT INTO `businfo` VALUES ('3', '宝马', NULL);

-- ----------------------------
-- Table structure for busparams
-- ----------------------------
DROP TABLE IF EXISTS `busparams`;
CREATE TABLE `busparams`  (
  `id` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `c_busName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '车名称',
  `c_busImage` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '车名称',
  `c_cs` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '厂商',
  `n_cszdj` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '厂商指导价',
  `d_sssj` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '上市时间',
  `c_zdgl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最大工具功率',
  `c_fdj` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发动机',
  `c_zdcs` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最大车速',
  `c_yh` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '油耗',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of busparams
-- ----------------------------
INSERT INTO `busparams` VALUES ('1', '卡宴', 't1.jpg', '卡宴', '166000', '2020-01', '550', '3.0T', '250', '6L');
INSERT INTO `busparams` VALUES ('2', '宝马', 't2.jpg', '宝马', '233333', '2020-01', '5555', '4.6T', '600', '7L');

-- ----------------------------
-- Table structure for cart
-- ----------------------------
DROP TABLE IF EXISTS `cart`;
CREATE TABLE `cart`  (
  `cart_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '购物车Id',
  `cart_product_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '购物车关联商品Id',
  `cart_user_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '购物车关联用户Id',
  `cart_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '购物车商品状态',
  `cart_check` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品是否选中',
  `cart_num` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '购物车数量',
  PRIMARY KEY (`cart_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of cart
-- ----------------------------
INSERT INTO `cart` VALUES ('062f0274a5ee4da8b147b94b3460b9ad', '1', '84182e6d9b434b98a4b0d9839ba5dd37', '1', '1', '3');
INSERT INTO `cart` VALUES ('269219a601fa41aa83c35fc5a6d033e5', '5', '2', '1', '1', '1');
INSERT INTO `cart` VALUES ('b03ae8d05a8f497e9b2e0214f61ac7b5', '4', '2', '1', '1', '1');
INSERT INTO `cart` VALUES ('ff7d188a38ee4457822b540dd69b4e36', '2', '84182e6d9b434b98a4b0d9839ba5dd37', '1', '1', '2');

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment`  (
  `id` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `pid` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父id',
  `comment` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '评论内容',
  `user_id` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '评论人',
  `create_time` datetime NULL DEFAULT NULL COMMENT '评论时间',
  `article_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文章ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of comment
-- ----------------------------
INSERT INTO `comment` VALUES ('1', '0', '一级组件', '84182e6d9b434b98a4b0d9839ba5dd37', '2021-05-03 14:05:38', '1');
INSERT INTO `comment` VALUES ('2', '1', '子组件', '84182e6d9b434b98a4b0d9839ba5dd37', '2021-05-03 14:06:26', '1');
INSERT INTO `comment` VALUES ('30fb6fcc68b145b48d440abbce184305', '1', '我来了', '84182e6d9b434b98a4b0d9839ba5dd37', '2021-05-03 15:46:28', NULL);
INSERT INTO `comment` VALUES ('6dea0a8f578441f88d803831ca355c7b', '0', '斯维尔无v', '2', '2021-05-10 03:43:40', '1');
INSERT INTO `comment` VALUES ('a15bdd359534455ea94b0791823db08b', '0', 'kaishi', '84182e6d9b434b98a4b0d9839ba5dd37', '2021-05-03 17:12:18', '2');
INSERT INTO `comment` VALUES ('a27fbbfe08044ce2899b84ea8c3d09b0', '0', 'njkk[嘻嘻]', '84182e6d9b434b98a4b0d9839ba5dd37', '2021-05-08 13:35:40', '1');
INSERT INTO `comment` VALUES ('bab326b7fb6240e1a4806d00636614b9', 'f0c096571e384b17b7026b11d95d96bf', '第三方第三方', '84182e6d9b434b98a4b0d9839ba5dd37', '2021-05-28 03:23:08', '1');
INSERT INTO `comment` VALUES ('c2d8af97f9894738a77dd4be3c12897e', '1', '我微信', '84182e6d9b434b98a4b0d9839ba5dd37', '2021-05-03 15:48:03', NULL);
INSERT INTO `comment` VALUES ('dcc779e5daaf4561a77a6ca8d1bcd185', '0', 'sssss', '84182e6d9b434b98a4b0d9839ba5dd37', '2021-05-03 17:09:02', '2');
INSERT INTO `comment` VALUES ('f0c096571e384b17b7026b11d95d96bf', '0', '啊啊啊', '2', '2021-05-10 05:38:29', '1');

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product`  (
  `product_id` int(11) NOT NULL COMMENT '商品ID\r\n',
  `product_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品名称',
  `product_price` int(11) NULL DEFAULT NULL COMMENT '商品售价',
  `product_num` int(11) NULL DEFAULT NULL COMMENT '商品库存',
  `product_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品展示图片',
  `product_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品所属类型',
  PRIMARY KEY (`product_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES (1, '卡宴', 1200000, 0, 't1.jpg', '1');
INSERT INTO `product` VALUES (2, '宝马', 2400000, 0, 't2.jpg', '1');
INSERT INTO `product` VALUES (3, '保时捷', 4600000, 4, 't3.jpg', '2');
INSERT INTO `product` VALUES (4, '野马', 600000, 6, 't4.jpg', '2');
INSERT INTO `product` VALUES (5, '星越', 120000, 5, 't5.jpg', '2');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键唯一',
  `c_userName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `c_password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户密码',
  `n_phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户电话',
  `c_avatar` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户图像',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('2', 'a', 'a', '112345689', 'https://cube.elemecdn.com/0/88/03b0d39583f48206768a7534e55bcpng.png');
INSERT INTO `user` VALUES ('84182e6d9b434b98a4b0d9839ba5dd37', 'w', 'w', '123456789', 'https://cube.elemecdn.com/0/88/03b0d39583f48206768a7534e55bcpng.png');

SET FOREIGN_KEY_CHECKS = 1;
