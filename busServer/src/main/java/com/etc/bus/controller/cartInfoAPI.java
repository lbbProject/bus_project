package com.etc.bus.controller;

import com.alibaba.fastjson.JSONObject;
import com.etc.bus.busDao.CartInfoDao;
import com.etc.bus.busDao.ProductDao;
import com.etc.bus.service.CartInfoService;
import com.etc.bus.util.response.RetCode;
import com.etc.bus.util.response.RetResult;
import com.etc.bus.util.tools;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping("/cart")
public class cartInfoAPI {

    @Resource
    private CartInfoService cartInfoService;

    /**
     * 根据传入的类型，获取对应的商品信息
     * @param params
     * @return
     */
    @PostMapping("/productInfo")
    public RetResult getProductInfo(@RequestBody JSONObject params){
        String productType = params.get("productType").toString();
        List<ProductDao> result =  cartInfoService.getProductInfo(productType);
        return new RetResult(RetCode.SUCCESS.getCode(), "获取商品信息", result);
    }

    /**
     * 更新商品状态
     * @param params
     * @return
     */
    @PostMapping("/updateById")
    public RetResult updateById(@RequestBody JSONObject params){
        String cartId = params.get("cartId").toString();
        String cartCheck = params.get("cartCheck").toString();
        Boolean result=  cartInfoService.updateById(cartId,cartCheck);
        return new RetResult(RetCode.SUCCESS.getCode(), "更新商品状态成功", result);
    }

    /**
     * 更新商品库存信息
     * @param params
     * @return
     */
    @PostMapping("/updateByNum")
    public RetResult updateByNum(@RequestBody JSONObject params){
        Object param = params.get("list");

        List<Map<String, Object>> mapList = (List<Map<String, Object>>) params.get("list");
        for(Map<String, Object> map : mapList){
            String oldNum =  getProductNum(map.get("productId").toString(),map.get("productType").toString());
            String productNum =String.valueOf (Integer.parseInt(oldNum) -(Integer.parseInt (map.get("productNum").toString()) ) );
            String productId = map.get("productId").toString();
            cartInfoService.updateByNum(productNum,productId);
        }

        return new RetResult(RetCode.SUCCESS.getCode(), "更新商品库存成功");
    }

    /**
     * 获取用户对应的购物车信息
     * @param params
     * @return
     */
    @PostMapping("/getCartInfo")
    public RetResult getCartInfo(@RequestBody JSONObject params){
        String userId = params.get("userId").toString();
        List<CartInfoDao> result =  cartInfoService.getCartInfo(userId);
        return new RetResult(RetCode.SUCCESS.getCode(), "获取购物车商品信息", result);
    }

    /**
     * 用户删除购物车数据
     * @param params
     * @return
     */
    @PostMapping("/deleteByCart")
    public  RetResult deleteByCart(@RequestBody JSONObject params){
        String cartId = params.get("cartId").toString();
        Boolean result = cartInfoService.deleteByCart(cartId);
        return new RetResult(RetCode.SUCCESS.getCode(), "删除成功", result);
    }

    /**
     * 用户添加购物车信息
     * @param params
     * @return
     */
    @PostMapping("/addCart")
    public  RetResult addCart(@RequestBody JSONObject params){
        //CartInfoDao cartInfoDao = (CartInfoDao) params.get("cartId");
        String uuID = new tools().getUUID();
        CartInfoDao cartInfoDao  = new CartInfoDao();
        cartInfoDao.setCartCheck(params.get("cartCheck").toString());
        cartInfoDao.setCartId(uuID);
        cartInfoDao.setCartProductId(params.get("cartProductId").toString());
        cartInfoDao.setCartUserId(params.get("cartUserId").toString());
        cartInfoDao.setCartStatus(params.get("cartStatus").toString());
        cartInfoDao.setCartNum("1");


        CartInfoDao oldCartInfo = cartInfoService.findCartById(params.get("cartProductId").toString(),params.get("cartUserId").toString());

        if(oldCartInfo== null ||  oldCartInfo.getCartId().equals("") ){
            Boolean result = cartInfoService.addCart(cartInfoDao);
            return new RetResult(RetCode.SUCCESS.getCode(), "添加成功", result);
        }else {
            String newNum =String.valueOf(Integer.parseInt(oldCartInfo.getCartNum()) +1);
            Boolean result =  cartInfoService.updateCartNumById(oldCartInfo.getCartId(),newNum);
            return new RetResult(RetCode.SUCCESS.getCode(), "添加成功", result);
        }

    }



    private String getProductNum(String productId,String ProductType){
        List<ProductDao> result =  cartInfoService.getProductInfo(ProductType);
        ProductDao temp = null;
        for(ProductDao item: result) {
            if(item.getProductId().equals(productId)){
                 temp =  item;
            }
        }
        return temp.getProductNum();
    }


}
