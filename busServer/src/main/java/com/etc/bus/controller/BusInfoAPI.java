package com.etc.bus.controller;

import com.alibaba.fastjson.JSONObject;
import com.etc.bus.busDao.BusInfoDao;
import com.etc.bus.busDao.BusParamInfo;
import com.etc.bus.busDao.CommentDao;
import com.etc.bus.service.BusInfoService;
import com.etc.bus.util.response.RetCode;
import com.etc.bus.util.response.RetResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/bus")
public class BusInfoAPI {
    @Resource
    private BusInfoService busInfoService;

    /**
     * 获取所有的车型信息
     * @return
     */
    @PostMapping("/busList")
    public RetResult getBusList(){
        List<BusInfoDao> result = busInfoService.getBusList();
        return new RetResult(RetCode.SUCCESS.getCode(), "所有车型信息",result);
    }

    /**
     * 获取具体某一款车型参数信息
     * @param params
     * @return
     */
    @PostMapping("/busParams")
    public RetResult getParamsList(@RequestBody JSONObject params){
        String busId = params.get("busId").toString();
        BusParamInfo result = busInfoService.getBusParamInfo(busId);
        return new RetResult(RetCode.SUCCESS.getCode(), "所有车型所有参数信息",result);
    }
}
