package com.etc.bus.controller;

import com.etc.bus.busDao.BusUseDao;
import com.etc.bus.service.UseService;
import com.etc.bus.util.response.RetCode;
import com.etc.bus.util.response.RetResult;
import com.etc.bus.util.tools;
import com.alibaba.fastjson.JSONObject;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@CrossOrigin
@RestController
@RequestMapping("/busInfo")
public class UserAPI {
    @Resource
    private UseService useService;
    //登录信息
    @PostMapping("/loginInfo")
    public RetResult getLoginInfo(@RequestBody JSONObject params) {
        if (params.get("username") == null || params.get("username").toString().isEmpty()) {
            return new RetResult(RetCode.FAIL.getCode(), "用户名不能为空");
        }

        BusUseDao  busUseDao = new BusUseDao();
        busUseDao.setPassword((String) params.get("passwodrd"));
        //登录用户名分两种情况 （默认 第一种输入用户名）
        busUseDao.setUserName((String) params.get("username"));
        BusUseDao resUseInfo =   useService.getUserByInfo(busUseDao);
        if(resUseInfo == null ){
            //第二种情况
            busUseDao.setUserName(null);
            busUseDao.setPhone(params.get("username").toString());
            resUseInfo =   useService.getUserByInfo(busUseDao);
            if(resUseInfo == null){
                //输入的用户账号不存在
                return new RetResult(RetCode.FAIL.getCode(), "用户输入账号不存在");
            }else {
                if (resUseInfo.getPassword().equals(params.get("password"))) {
                    return new RetResult(RetCode.SUCCESS.getCode(), "用户登录成功", resUseInfo);
                } else {
                    return new RetResult(RetCode.FAIL.getCode(), "用户名密码错误");
                }

            }
        }else {
            if (resUseInfo.getPassword().equals(params.get("password"))) {
                return new RetResult(RetCode.SUCCESS.getCode(), "用户登录成功", resUseInfo);
            } else {
                return new RetResult(RetCode.FAIL.getCode(), "用户名密码错误");
            }
        }
    }
    //登录信息
    @PostMapping("/registry")
    public RetResult postRegistry(@RequestBody JSONObject params) {
        String uuID = new tools ().getUUID();
        BusUseDao  busUseDao = new BusUseDao();
        busUseDao.setUserName((String) params.get("userName"));
        BusUseDao resUseInfo = useService.getUserByInfo(busUseDao);
        if(resUseInfo == null){
            busUseDao.setUserName(null);
            busUseDao.setPhone(params.get("phone").toString());
            resUseInfo = useService.getUserByInfo(busUseDao);
            if(resUseInfo == null){
                busUseDao.setId(uuID);
                busUseDao.setUserName((String) params.get("userName"));
                busUseDao.setPassword((String) params.get("password"));
                Boolean res =  useService.insertUserInfo(busUseDao);

                if(res){
                    return new RetResult(RetCode.SUCCESS.getCode(), "用户注册成功", res);
                }else {
                    return new RetResult(RetCode.FAIL.getCode(), "用户注册失败");
                }
            }else {
                return new RetResult(RetCode.FAIL.getCode(), "手机号已存在");
            }

        }else {

            return new RetResult(RetCode.FAIL.getCode(), "用户名已存在");
        }
    }


}
