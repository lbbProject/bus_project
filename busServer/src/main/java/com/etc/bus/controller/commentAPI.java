package com.etc.bus.controller;


import com.alibaba.fastjson.JSONObject;
import com.etc.bus.busDao.CommentDao;
import com.etc.bus.service.CommentService;
import com.etc.bus.util.response.RetCode;
import com.etc.bus.util.response.RetResult;
import com.etc.bus.util.tools;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

@CrossOrigin(origins = "*",maxAge = 3600)
@RestController
@RequestMapping("/commentInfo")
public class commentAPI {
    @Resource
    private CommentService commentService;

    /**
     * 根据类型获取对应的评论数据
     * @param params
     * @return
     */
    @PostMapping("/getCommentByArticleId")
    public RetResult getCommentByArticle(@RequestBody JSONObject params){
        String Id = params.get("articleId").toString();
        List<CommentDao> result=  commentService.getCommentByArticle(Id);
        return new RetResult(RetCode.SUCCESS.getCode(), "文章评论信息", result);
    }

    /**
     * 添加评论
     * @param params
     * @return
     * @throws ParseException
     */
    @PostMapping("/addComment")
    public RetResult addComment(@RequestBody JSONObject params) throws ParseException {
        CommentDao commentDao  =new CommentDao();
        String uuID = new tools().getUUID();
        commentDao.setId(uuID);
        commentDao.setContent(params.get("content").toString());
        commentDao.setUserId(params.get("userId").toString());
        commentDao.setPid(params.get("pid").toString());
        commentDao.setArticleId(params.get("articleId").toString());
        commentDao.setCreateDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(new tools().currenDate())) ;
        Integer result =  commentService.addComment(commentDao);
        if(result > 0){
            return new RetResult(RetCode.SUCCESS.getCode(), "评论添加成功", result);
        }else {
            return new RetResult(RetCode.FAIL.getCode(), "评论添加失败");
        }
    }
}
