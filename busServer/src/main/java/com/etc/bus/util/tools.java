package com.etc.bus.util;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

public class tools {
    /**
     * 获取唯一ID
     * @return
     */
    public  String getUUID(){
        return UUID.randomUUID().toString().replace("-", "");
    }

    public String currenDate(){
        //使用默认时区和语言环境获得一个日历。
        Calendar rightNow    =    Calendar.getInstance();
		/*用Calendar的get(int field)方法返回给定日历字段的值。
		HOUR 用于 12 小时制时钟 (0 - 11)，HOUR_OF_DAY 用于 24 小时制时钟。*/
        Integer year = rightNow.get(Calendar.YEAR);
        Integer month = rightNow.get(Calendar.MONTH)+1; //第一个月从0开始，所以得到月份＋1
        Integer day = rightNow.get(rightNow.DAY_OF_MONTH);
        Integer hour12 = rightNow.get(rightNow.HOUR);
        Integer hour24 = rightNow.get(rightNow.HOUR_OF_DAY);
        Integer minute = rightNow.get(rightNow.MINUTE);
        Integer second = rightNow.get(rightNow.SECOND);
        Integer millisecond = rightNow.get(rightNow.MILLISECOND);
        String TimeNow24 = year+"-"+month+"-"+day+" "+hour24+":"+minute+":"+second+":"+millisecond;
        return TimeNow24;
    }
}
