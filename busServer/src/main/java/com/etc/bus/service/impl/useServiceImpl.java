package com.etc.bus.service.impl;

import com.etc.bus.busDao.BusUseDao;
import com.etc.bus.busDao.mapper.UserMapper;
import com.etc.bus.service.UseService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
@Service
public class useServiceImpl  implements UseService {
    @Resource
    private UserMapper userMapper;
    @Override
    public BusUseDao getUserByInfo(BusUseDao busUseDao) {
        BusUseDao userInfo =   userMapper.getUserByInfo(busUseDao);
        return userInfo;
    }

    @Override
    public Boolean insertUserInfo(BusUseDao busUseDao) {
        Integer res =   userMapper.insertUser(busUseDao);
        return res==1?true:false;
    }
}
