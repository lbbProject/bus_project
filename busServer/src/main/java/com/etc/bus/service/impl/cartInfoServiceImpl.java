package com.etc.bus.service.impl;

import com.etc.bus.busDao.CartInfoDao;
import com.etc.bus.busDao.ProductDao;
import com.etc.bus.busDao.mapper.CartInfoMapper;
import com.etc.bus.service.CartInfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class cartInfoServiceImpl implements CartInfoService {

    @Resource
    private CartInfoMapper cartInfoMapper;
    @Override
    public List<ProductDao> getProductInfo(String productType) {
        List<ProductDao> result = cartInfoMapper.getProductInfo(productType);
        return result;
    }

    @Override
    public Boolean updateById(String cartId, String cartCheck) {
        Integer result = cartInfoMapper.updateById(cartId,cartCheck);
        return result>0?true:false;
    }

    @Override
    public Boolean updateCartNumById(String cartId, String cartNum) {
        Integer result = cartInfoMapper.updateCartNumById(cartId,cartNum);
        return result>0?true:false;
    }

    @Override
    public CartInfoDao findCartById(String productId,String userId) {
        CartInfoDao result = cartInfoMapper.findCartById(productId,userId);
        return result;
    }

    @Override
    public Boolean updateByNum(String productNum, String productId) {
        Integer result = cartInfoMapper.updateByNum(productNum,productId);
        return result>0?true:false;
    }

    @Override
    public List<CartInfoDao> getCartInfo(String userId) {
        List<CartInfoDao> result = cartInfoMapper.getCartInfo(userId);
        return result;
    }

    @Override
    public Boolean deleteByCart(String cartId) {
        Integer result = cartInfoMapper.deleteByCartId(cartId);

        return result>0?true:false;
    }

    @Override
    public Boolean addCart(CartInfoDao cartInfoDao) {
        Integer result =  cartInfoMapper.addCart(cartInfoDao);
        return result>0?true:false;
    }
}
