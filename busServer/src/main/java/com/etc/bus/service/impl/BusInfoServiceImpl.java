package com.etc.bus.service.impl;

import com.etc.bus.busDao.BusInfoDao;
import com.etc.bus.busDao.BusParamInfo;
import com.etc.bus.busDao.mapper.BusInfoMapper;
import com.etc.bus.service.BusInfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class BusInfoServiceImpl implements BusInfoService {

    @Resource
    private BusInfoMapper busInfoMapper;
    @Override
    public List<BusInfoDao> getBusList() {
        List<BusInfoDao> result = busInfoMapper.getBusType();
        return result;
    }

    @Override
    public BusParamInfo getBusParamInfo(String busId) {
        BusParamInfo result = busInfoMapper.getBusParamById(busId);
        return result;
    }
}
