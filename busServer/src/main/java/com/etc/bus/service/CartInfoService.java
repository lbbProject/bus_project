package com.etc.bus.service;

import com.etc.bus.busDao.CartInfoDao;
import com.etc.bus.busDao.ProductDao;

import java.util.List;

public interface CartInfoService {
    // 获取商品信息

    /**
     * 获取商品信息接口
     * @param productType
     * @return
     */
    List<ProductDao> getProductInfo(String productType);

    /**
     * 更新商品状态接口
     * @param cartId
     * @param cartCheck
     * @return
     */
    Boolean updateById(String cartId,String cartCheck);

    /**
     * 更新商品库存接口
     * @param productNum
     * @param productId
     * @return
     */
    Boolean updateByNum(String productNum,String productId);

    /**
     * 获取购物车信息
     * @param userId
     * @return
     */
    List<CartInfoDao> getCartInfo(String userId);

    /**
     * 删除购物车数据
     * @param cartId
     * @return
     */
    Boolean deleteByCart(String cartId);

    /**
     * 添加购物车
     * @param cartInfoDao
     * @return
     */
    Boolean addCart(CartInfoDao cartInfoDao);


    Boolean updateCartNumById(String cartId,String cartNum);

    CartInfoDao findCartById(String productId,String userId);
}
