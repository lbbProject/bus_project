package com.etc.bus.service;

import com.etc.bus.busDao.BusInfoDao;
import com.etc.bus.busDao.BusParamInfo;

import java.util.List;

public interface BusInfoService {
    /**
     * 获取车型接口
     * @return
     */
    List<BusInfoDao> getBusList();

    /**
     * 获取车型参数
     * @param busId
     * @return
     */
    BusParamInfo getBusParamInfo(String busId);


}
