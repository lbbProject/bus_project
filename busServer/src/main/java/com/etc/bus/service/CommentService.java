package com.etc.bus.service;

import com.etc.bus.busDao.CommentDao;

import java.util.List;

public interface CommentService {
    //获取文章评论信息

    /**
     * 获取文章评论信息
     * @param articleId
     * @return
     */
    List<CommentDao> getCommentByArticle(String articleId);

    //添加评论信息

    /**
     * 添加评论信息
     * @param commentDao
     * @return
     */
    Integer addComment(CommentDao commentDao);
}
