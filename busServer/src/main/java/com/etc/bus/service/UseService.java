package com.etc.bus.service;

import com.etc.bus.busDao.BusUseDao;

public interface UseService {

    //获取登录信息
    BusUseDao getUserByInfo(BusUseDao busUseDao);

    //注册接口
    Boolean insertUserInfo(BusUseDao busUseDao);
}
