package com.etc.bus.service.impl;

import com.etc.bus.busDao.CommentDao;
import com.etc.bus.busDao.mapper.CommentMapper;
import com.etc.bus.service.CommentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class commentServiceImpl implements CommentService {

    @Resource
    private CommentMapper commentMapper;
    @Override
    public List<CommentDao> getCommentByArticle(String articleId) {
        List<CommentDao> result = commentMapper.getCommentInfo(articleId);
        return result;
    }

    @Override
    public Integer addComment(CommentDao commentDao) {
        Integer result =  commentMapper.addComment(commentDao);
        return result;
    }


}
