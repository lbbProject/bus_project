package com.etc.bus.busDao.mapper;

import com.etc.bus.busDao.BusUseDao;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface UserMapper {

    //获取登录人信息
    //@Select("select id,c_userName as userName,c_password as password,n_phone as phone from busdata.user where c_userName=#{userName} or n_phone=#{phone}")
    @Select({
            "<script>",
            "select id,c_userName as userName,c_password as password,n_phone as phone,c_avatar as avatar from busdata.user where 1=1",
            "<when test='userName!=null'>",
            "and c_userName=#{userName}",
            "</when>",
            "<when test='phone!=null'>",
            "and n_phone=#{phone}",
            "</when>",
            "</script>"
    })
    BusUseDao getUserByInfo(BusUseDao busUseDao);
    //注册用户信息

    @Insert("insert into busdata.user values (#{id},#{userName},#{password},#{phone}) ")
    Integer insertUser(BusUseDao busUseDao);

    @Select("select id,c_userName as userName,c_avatar as avatar from busdata.user where id=#{Id}")
    BusUseDao findById (String Id);
}
