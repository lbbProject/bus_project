package com.etc.bus.busDao;

import lombok.Data;

@Data
public class BusInfoDao {
    private String busId;

    private String busName;

}
