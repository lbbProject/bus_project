package com.etc.bus.busDao.mapper;

import com.etc.bus.busDao.CommentDao;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.util.List;

@Mapper
public interface CommentMapper {
    @Select("select * from busdata.comment where article_id = #{articleId}")
    @Results(id = "commentResult",value = {
            @Result(id = true,property ="id",column = "id"),
            @Result(property ="pid",column = "pid"),
            @Result(property ="content",column = "comment"),
            @Result(property ="createDate",column = "create_time"),
            @Result(property ="commentUser",column = "user_id",one = @One(select = "com.etc.bus.busDao.mapper.UserMapper.findById",fetchType = FetchType.EAGER))

    })
    List<CommentDao> getCommentInfo(String articleId);

    @Insert("insert into busdata.comment values(#{id},#{pid},#{content},#{UserId},#{createDate},#{articleId})")
    Integer addComment(CommentDao commentDao);
}
