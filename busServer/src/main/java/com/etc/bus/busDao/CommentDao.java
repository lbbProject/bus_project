package com.etc.bus.busDao;

import lombok.Data;

import java.util.Date;

@Data
public class CommentDao {
    private  String id;
    private  String pid;
    private String content;
    private BusUseDao commentUser;
    private Date createDate;
    private  String articleId;
    private  String UserId;
}
