package com.etc.bus.busDao.mapper;

import com.etc.bus.busDao.CartInfoDao;
import com.etc.bus.busDao.ProductDao;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CartInfoMapper {

    // 获取所有的商品信息
    @Select("select product_id as productId,product_name as productName,product_price as productPrice,product_num as productNum, product_img as productImg from busdata.product where product_type = #{productType}")
    List<ProductDao> getProductInfo(String productType);
    //更新购物车商品状态
    @Update("update busdata.cart set cart_check =#{cartCheck} where cart_id = #{cartId}")
    Integer updateById(String cartId,String cartCheck);

    @Update("update busdata.cart set cart_num =#{cartNum} where cart_id = #{cartId}")
    Integer updateCartNumById(String cartId,String cartNum);

    //更新商品库存
    @Update("update busdata.product set product_num =#{productNum} where product_id = #{productId}")
    Integer updateByNum(String productNum,String productId);


    //获取购物车数据
    @Select("select \n" +
            "product_id as productId,\n" +
            "product_name as productName,\n" +
            "product_price as productPrice,\n" +
            "product_num as productNum,\n" +
            "product_img as productImg,\n" +
            "product_type as productType,\n" +
            "cart_id as cartId,\n" +
            "cart_status as cartStatus,\n" +
            "cart_check as cartCheck,\n" +
            "cart_Num as cartNum\n" +
            "from busdata.product p,busdata.cart c where p.product_id = c.cart_product_id and cart_user_id=#{userId} and cart_status = '1'")
    List<CartInfoDao> getCartInfo(String userId);

    /**
     * 删除购物车数据
     * @param CartID
     * @return
     */
    @Delete("delete from busdata.cart where cart_id=#{CartID}")
    Integer deleteByCartId(String CartID);

    /**
     * 添加购物车信息
     * @param cartInfoDao
     * @return
     */
    @Insert("insert into busdata.cart values(#{cartId},#{cartProductId},#{cartUserId},#{cartStatus},#{cartCheck})")
    Integer addCart(CartInfoDao cartInfoDao);

    @Select("select cart_id as cartId,cart_status as cartStatus,cart_check as cartCheck,cart_Num as cartNum from busdata.cart where cart_product_id=#{productId} and cart_user_id=#{userId}")
    CartInfoDao findCartById(String productId,String userId);

}
