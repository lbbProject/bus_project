package com.etc.bus.busDao;

import lombok.Data;

@Data
public class BusParamInfo {
    private String id;
    private String name;

    private String busImage;

    private String cs;

    private String cszdj;

    private String sssj;

    private String zdgl;

    private String fdj;

    private String zdcs;

    private String yh;


}
