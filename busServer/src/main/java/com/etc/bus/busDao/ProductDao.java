package com.etc.bus.busDao;

import lombok.Data;

@Data
public class ProductDao {
    private String productId;

    private String productName;

    private String productPrice;

    private String productNum;

    private String productImg;

    private String productType;
}
