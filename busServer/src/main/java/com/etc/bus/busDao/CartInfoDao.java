package com.etc.bus.busDao;

import lombok.Data;

@Data
public class CartInfoDao extends ProductDao {
    private String cartId;

    private String cartProductId;

    private String cartUserId;

    private String cartStatus;

    private String cartCheck;

    private String cartNum;
}
