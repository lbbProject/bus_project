package com.etc.bus.busDao.mapper;

import com.etc.bus.busDao.BusInfoDao;
import com.etc.bus.busDao.BusParamInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface BusInfoMapper {
    /**
     * 获取所有的车型
     * @return
     */
    @Select("select id as busId,c_busName as busName from busdata.busParams")
    List<BusInfoDao> getBusType();

    /**
     * 获取对应的车型参数
     * @param busId
     * @return
     */
    @Select("select id, c_busName as name,c_busImage as busImage,c_cs as cs,n_cszdj as cszdj,d_sssj as sssj,c_zdgl as zdgl,c_fdj as fdj,c_zdcs as zdcs,c_yh as yh from busdata.busParams where id=#{busId}")
    BusParamInfo getBusParamById(String busId);
}
