package com.etc.bus.busDao;

import lombok.Data;

@Data
public class BusUseDao {
    private  String id;

    private  String userName;

    private String password;

    private String phone;

    private String avatar;

}
